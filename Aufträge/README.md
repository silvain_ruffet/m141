| **Tätigkeit**                         | **SQL-Befehl**                                               | **Gruppe** | **Warnung** |
|---------------------------------------|--------------------------------------------------------------|---------|-------------|
| 1) alle Daten einer Tabelle anzeigen  | `SELECT * FROM tablename;`                                   | DML     |             |
| 2) Datenbank auswählen                | `USE databasename;`                                          | DDL     |             |
| 3) eine neue Datenbank erstellen      | `CREATE DATABASE databasename;`                              | DDL     |             |
| 4) eine neue Tabelle erstellen        | `CREATE TABLE tablename (column1 datatype, column2 datatype);`| DDL    |             |
| 5) eine Tabelle löschen               | `DROP TABLE tablename;`                                      | DDL     | <img src="../Bilder/caution.png" width="20" height="20"> |
| 6) Tabellenstruktur kontrollieren     | `DESCRIBE tablename;` or `SHOW COLUMNS FROM tablename;`      | DDL     |             |
| 7) Datenbanken anzeigen               | `SHOW DATABASES;`                                            | DDL     |             |
| 8) Tabellen einer DB anzeigen         | `SHOW TABLES;`                                               | DDL     |             |
| 9) Daten in eine Tabelle eintragen    | `INSERT INTO tablename (column1, column2) VALUES (value1, value2);` | DML |             |
| 10) Daten in einer Tabelle ändern     | `UPDATE tablename SET column1 = value1 WHERE condition;`     | DML     | <img src="../Bilder/caution.png" width="20" height="20"> |
| 11) Daten in einer Tabelle löschen    | `DELETE FROM tablename WHERE condition;`                     | DML     | <img src="../Bilder/caution.png" width="20" height="20"> |
| 12) einen Index löschen               | `DROP INDEX indexname;`                                      | DDL     | <img src="../Bilder/caution.png" width="20" height="20"> |
| 13) einen Index erstellen             | `CREATE INDEX indexname ON tablename (columnname);`          | DDL     |             |
| 14) Spalte in einer Tabelle löschen   | `ALTER TABLE tablename DROP COLUMN columnname;`              | DDL     | <img src="../Bilder/caution.png" width="20" height="20"> |


- Sublangages SQL

DDL - Data Definition Line
Ist eine Sprache zum Beschreiben von Daten und ihrer Beziehungen in einer Datenbank. 

DML - Data Manipulation Line
Ist der Teil einer Datenbanksprache, die verwendet wird, um Daten zu schreiben, zu lesen, zu ändern und zu löschen.

DCL - Data Controll Line
Ist derjenige Teil einer Datenbanksprache, der verwendet wird, um Berechtigungen zu vergeben oder zu entziehen. DCL ist die Datenüberwachungssprache einer Datenbank.